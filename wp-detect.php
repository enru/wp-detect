<?php
/*
Plugin Name: WP Detect
Plugin URI: http://enru.co.uk/wp-detect
Description: Detects WordPress
Version: 0.1
Author: Neill Russell (@enru)  
Author URI: http://enru.co.uk/

Copyright: © 2014 Enru Technology
License: GNU General Public License v2.0
License URI: http://www.gnu.org/old-licenses/gpl-2.0.html
*/

require_once dirname(__FILE__).'/classes/class-wp-detection.php';

add_shortcode('wpdetect', function($atts) {
      $atts = shortcode_atts( array(
      ), $atts );

      $html = '<form class="wpdetect">';
      $html .= '<textarea id="wpdetect-sites" rows="10" cols="30" name="sites">'; //</textarea>';
      $html .= "http://inigo.net\n";
      $html .= "http://modahaus.com\n";
      $html .= "http://frydman.co.uk\n";
      $html .= '</textarea>';
      $html .= '<input type="submit" value="detect" />';
      $html .= '</form><div id="wpdetect-results"></div>';
      $html .= "
<script>
jQuery(function($) {

    $('.wpdetect').bind('submit', function(e) {
        e.stopPropagation()
        e.preventDefault()
        $('#wpdetect-sites').css('background-color', 'grey');
        $('#wpdetect-results').html('getting results...');
        var data = {
            'action': 'wpdetect',
            'sites': $('#wpdetect-sites').val()
        };
        $.post('/wp-admin/admin-ajax.php', data, function(response) {
            $('#wpdetect-results').empty();
            for(site in response) {
                var results = $('<dl></dl>');
                for(result in response[site]) {
                    $(results).append('<dt>'+result+'</dt><dd>'+response[site][result]+'</dd>');
                }
                $('#wpdetect-results').append('<p>'+site+': '+results.html()+'</p><hr/>');
            }
            $('#wpdetect-sites').css('background-color', 'white');
        });
    });
});
</script>
      ";

      return $html;
});

add_action('wp_ajax_wpdetect', 'wp_detect');
add_action('wp_ajax_nopriv_wpdetect', 'wp_detect');

function wp_detect() {

    $response = array();

    foreach(explode("\n", $_POST['sites']) as $site) {
        if($site = trim($site)) {
            $res = new WP_Detection($site);
            $response[$site]['wordpress'] = $res->isWordPress();
            if($res->isWordPress()) {
                $response[$site]['version'] = $res->getVersion();
                $response[$site]['method'] = $res->getMethod();
            }
        }
    }

    header('Content-type: application/json');
    echo json_encode($response);
    die();
}
