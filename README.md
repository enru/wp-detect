# wp-detect

A WordPress plugin that provides a shortcode that generates a form which can detect if a site at a given URL is running on WordPress or not.

Uses finger printing to detect certain files WordPress uses.

Original code found here:

http://wordpress.stackexchange.com/a/10641o

and here:

http://ottodestruct.com/class-wp-detection.phps

Modications made are: extra finger printing code added and wrapped in a WordPress shortcode/form.